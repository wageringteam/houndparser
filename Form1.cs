﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using HtmlAgilityPack;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace HoundParser
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            InitializeComponent();
        
        }

        string currentDirectory;
        ///Ryan 03/06/2019 - I've use ChromeDriver - because i dont have firfox in my machine
        ///update NuGet Package - Selenium.Chrome.WebDriver
        ChromeDriver driver; 
        private void Form1_Load(object sender, EventArgs e)
        {
            txtURL.Text = "http://greyhoundsform.betfair.com/racingform";
           
            string appStartUp = Application.StartupPath;
            currentDirectory = Environment.CurrentDirectory;
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string userSavePath = Properties.Settings.Default.savePath;
            tSavePath.Text = userSavePath;
            if (tSavePath.Text == "Choose your save path")
            {
                tSavePath.Text = currentDirectory;
                bSavePath.PerformClick();
            }
        }

        private void BtnParse_Click(object sender, EventArgs e)
        {
            ReadRaces();
        }

        private void ReadRaces()
        {
            driver = new ChromeDriver();
            ClearErrors();
            string dDate = "";

            ///Switch for headless mode = browser don`t show.
            //FirefoxOptions option = new FirefoxOptions();
            //option.AddArgument("--headless");
            //var driver = new FirefoxDriver(option);

            ///This will make browser visible (a little bit slower)
            ///update NuGet Package - Selenium.Firefox.WebDriver
            //var driver = new FirefoxDriver();

          

            //Opening Page and making sure its loaded fully, all asyncs
            driver.Navigate().GoToUrl("http://greyhoundsform.betfair.com/racingform");

            //We will wait for document to be ready, but it dont work on landing
            IWait<IWebDriver> wait2 = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait2.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            //We use task delay, only timed delay, just when page is loaded, to make sure all is done.
            Task.Delay(Int32.Parse(txtMeetingDelay.Text) * 1000).Wait();


            try
            {

                //Making structure shell - Racecard
                RaceCard rc = new RaceCard();
                rc.Date = "Now";
                rc.MeetingsNo = "0";
                rc.Meetings = new List<Meeting>();

                //WaitForJqueryAjax();
                //Select Combobox Meetings
                var meeting = driver.FindElementById("meetings");
                var selectMeetings = new SelectElement(meeting);

                //Add Some basic values for the root node RaceCard 
                rc.Date = DateTime.Today.Day.ToString() + "." + DateTime.Today.Month.ToString();
                rc.MeetingsNo = selectMeetings.Options.Count.ToString();

                for (int i = 1; i < selectMeetings.Options.Count; i++)
                //for (int i = 1; i < 3; i++) - for faster testing on smaller sets select onlyt first meeting
                {

                    
                    //We shall reload the control in each loop due to DOM stalness possibility.
                    //Also we select index 1 as start one then loop till the end.
                    selectMeetings = new SelectElement(driver.FindElementById("meetings"));
                    //WaitForJqueryAjax();
                    selectMeetings.SelectByIndex(i);

                    //First wait for the page to be completely loaded. This handles previosely made ajax call load
                    IWait<IWebDriver> wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(10));
                    wait.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));

                    //Once we get all loaded we have opened a meeting and a list of its races, with 1st race displayed.
                    //Declare shell for meeting
                    Meeting m = new Meeting();
                    m.Races = new List<Race>();
                    WaitForJqueryAjax(); //This one solves the DOM stall and delay. 
                    //Select races combo and control it in order to read race by race.
                    var race = driver.FindElementById("races");
                    var selectRaces = new SelectElement(race);

                    for (int z = 1; z < selectRaces.Options.Count; z++)
                    {

                        //make browser select race 1, 0 is combo title
                        //reload the control in each pass due to stall         
                        selectRaces = new SelectElement(driver.FindElementById("races"));
                        //WaitForJqueryAjax();
                        selectRaces.SelectByIndex(z);

                        //First wait for the page to be completely loaded.
                        IWait<IWebDriver> wait3 = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        wait3.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));

                        //We define shell for a race
                        Race r = new Race();
                        r.Runners = new List<Runner>();

                        //We read runners in HTMLAgility doc, this way we dont rape the DOM.
                        IWebElement runners = driver.FindElementByClassName("pageContent");
                        string innerHtml = runners.GetAttribute("innerHTML");
                        HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                        doc.LoadHtml(innerHtml);

                        
                        HtmlNode node = doc.DocumentNode.SelectSingleNode("//*[@id=\"raceTitle\"] ");
                        string value = (node == null) ? "Error, id - raceTitle not found" : node.InnerHtml;

                        string[] raceTitleChopName;
                        string[] raceTitleChopDate;
                        string[] raceTitleChopDistanceTime;

                        try
                        {
                            raceTitleChopName = value.Split(',');
                            raceTitleChopDate = raceTitleChopName[1].Split('-');
                            raceTitleChopDistanceTime = raceTitleChopDate[1].Split(' ');

                            //If we are in 1st race we will fill in meeting details. 
                            //Placing it here makes app read DOM one time less.
                            if (z == 1)
                            {
                                m.Name = raceTitleChopName[0] ?? string.Empty;
                                m.Date = raceTitleChopDate[0];
                                //Making Required Date
                                m.Date = m.Date.Replace("th", "");
                                m.Date = m.Date.Replace("nd", "");
                                m.Date = m.Date.Replace("st", "");
                                m.Date += DateTime.Now.Year.ToString();
                                m.JumpTime = raceTitleChopDistanceTime[1] ?? string.Empty;
                                dDate = m.Date;
                            }

                            //Fill in race data
                            r.PostTime = raceTitleChopDistanceTime[1] ?? string.Empty;
                            r.Class = raceTitleChopDistanceTime[2] ?? string.Empty;
                            r.Distance = raceTitleChopDistanceTime[3] ?? string.Empty;
                        }
                        catch (Exception ex)
                        {
                            WriteError("Spliting race title, unexpected input", ex);
                        }
                     

                        raceTitleChopName = null;
                        raceTitleChopDate = null;
                        raceTitleChopDistanceTime = null;


                        var allElementsWithClassFloat = doc.DocumentNode.SelectNodes("//*[contains(@class,'header dog')]");

                        foreach (HtmlNode div in allElementsWithClassFloat)
                        {
                            //New runner
                            var run = new Runner();
                            run.Forma = new List<Forma>();
                            run.Number = div.Attributes["class"].Value.ToString();
                            run.Number = run.Number[run.Number.Length - 1].ToString();
                            //If we get X name some error happened in parsing runners
                            run.Name = "X";
                            //Isolating split errors
                            try
                            {
                                //Fetching only H4 tag hound name.
                                var justHoundNamesH4 = div.SelectNodes(".//h4");
                                run.Name = justHoundNamesH4[0].InnerText;
                                justHoundNamesH4 = null;

                                var trainer = div.SelectNodes(".//div[contains(@class, 'trainer')]");
                                run.Trainer = trainer[0].InnerText.Replace("Trainer:", "");
                                trainer = null;

                                var breeding = div.SelectNodes(".//div[contains(@class, 'breeding')]");
                                string breed = breeding[0].InnerText;
                                string[] sireDamChop = breed.Split('-');
                                string[] DOBChop = sireDamChop[1].Split('(');
                                run.Sire = sireDamChop[0].Trim();
                                run.Dam = DOBChop[0].Trim();
                                run.DOB = DOBChop[1].Replace(')', ' ').Trim();

                                //We need to fetch next 5 TR's, from div's parent

                                HtmlNode runParentTR = div.SelectSingleNode("..");
                                //        //    ////we take next 5 <TR>'s as they are last 5 results for runner
                                HtmlNode nextElement = runParentTR;
                                for (int g = 0; g < 5; g++)
                                {

                                    Forma f = new Forma();
                                    nextElement = nextElement.SelectSingleNode("following-sibling::*");
                                    f.Date = nextElement.SelectSingleNode(".//*[contains(@class, 'formattedDate')]").InnerText;
                                    f.Distance = nextElement.SelectSingleNode(".//*[contains(@class, 'distance')]").InnerText;
                                    f.RaceNumber = nextElement.SelectSingleNode(".//*[contains(@class, 'trapNo')]").InnerText;
                                    f.Sectional = nextElement.SelectSingleNode(".//*[contains(@class, 'sectionalTime')]").InnerText;
                                    f.Peer = nextElement.SelectSingleNode(".//*[contains(@class, 'bendPosition')]").InnerText;
                                    f.FP = nextElement.SelectSingleNode(".//*[contains(@class, 'position')]").InnerText;
                                    f.Comment = nextElement.SelectSingleNode(".//*[contains(@class, 'comment')]").InnerText;
                                    f.WinnerTime = nextElement.SelectSingleNode(".//*[contains(@class, 'winningTime')]").InnerText;
                                    f.TimeAdj = nextElement.SelectSingleNode(".//*[contains(@class, 'goingAllowance')]").InnerText;
                                    f.Class = nextElement.SelectSingleNode(".//*[contains(@class, 'racingClass')]").InnerText;
                                    f.RunnerTime = nextElement.SelectSingleNode(".//*[contains(@class, 'adjustedTime')]").InnerText;
                                    run.Forma.Add(f);
                                    f = null;

                                }
                            }
                            catch (Exception ex)
                            {
                                WriteError("Parsing breeding, unexpected input.", ex);
                            }

                            r.Runners.Add(run);
                            run = null;
                        }
                        m.Races.Add(r);
                        r = null;
                    }

                    XmlSerializer pxsSubmit = new XmlSerializer(typeof(Meeting));
                    var psubReq = m;
                    var pxml = "";

                    using (var sww = new StringWriter())
                    {
                        using (XmlWriter writer = XmlWriter.Create(sww))
                        {
                            pxsSubmit.Serialize(writer, psubReq);
                            pxml = sww.ToString(); // Your XML
                            XmlDocument xdoc = new XmlDocument();
                            xdoc.LoadXml(pxml);
                            string path = tSavePath.Text + m.Date + "_" + m.Name + "_" + m.JumpTime.Replace(":", "-").Replace(" ", "") + ".xml";
                            xdoc.Save(path); ;
                            //Just to try
                        }
                    }

                    rc.Meetings.Add(m);
                    m = null;
                }


                XmlSerializer xsSubmit = new XmlSerializer(typeof(RaceCard));
                var subReq = rc;
                var xml = "";

                using (var sww = new StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sww))
                    {
                        xsSubmit.Serialize(writer, subReq);
                        xml = sww.ToString(); // Your XML
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.LoadXml(xml);
                        string path = tSavePath.Text + dDate + "_all.xml";
                        xdoc.Save(path);
                    }
                }
                txtOutput.Text = xml;
                tabControl1.SelectTab(1);
                driver.Quit();


            }
            catch (Exception ex)
            {
                WriteError("ReadingMeetings", ex);
                driver.Quit();
            }



            //// Read it back in
            //RaceCard dataFromFile = null;
            //using (XmlReader reader = XmlTextReader.Create("customer.xml"))
            //{
            //    dataFromFile = (CustomerData)serializer.Deserialize(reader);
            //}

            // txtOutput.Text = rc.;

        }

        private void WriteError(string error, Exception ex)
        {

            txtErrorLog.Text += error + ex.Message;

        }
        private void ClearErrors()
        {

            if (txtErrorLog.InvokeRequired)
            {
                m_ClearErrLogText ClrLabel = ClearErrorLogText;
                Invoke(ClrLabel);
            }
            else
                txtErrorLog.Clear();


        }
        delegate void m_ClearErrLogText();
        private void ClearErrorLogText()
        {
            txtErrorLog.Clear();
        }


        private bool IsValidPath(string path, bool allowRelativePaths = false)
        {
            bool isValid = true;

            try
            {
                string fullPath = Path.GetFullPath(path);

                if (allowRelativePaths)
                {
                    isValid = Path.IsPathRooted(path);
                }
                else
                {
                    string root = Path.GetPathRoot(path);
                    isValid = string.IsNullOrEmpty(root.Trim(new char[] { '\\', '/' })) == false;
                }
            }
            catch (Exception ex)
            {
                isValid = false;
            }

            return isValid;
        }

        public void WaitForJqueryAjax()
        {
            int delay = 3;
            while (delay > 0)
            {
                Thread.Sleep(1000);
                var jquery = (bool)(this.driver as IJavaScriptExecutor)
                    .ExecuteScript("return window.jQuery == undefined");
                if (jquery)
                {
                    break;
                }
                var ajaxIsComplete = (bool)(this.driver as IJavaScriptExecutor)
                    .ExecuteScript("return window.jQuery.active == 0");
                if (ajaxIsComplete)
                {
                    break;
                }
                delay--;
            }
        }

        private void BGetAppPAth_Click(object sender, EventArgs e)
        {
            tSavePath.Text = currentDirectory;
        }
        
        private void bSavePath_Click(object sender, EventArgs e)
        {
            if (IsValidPath(tSavePath.Text))
            {
                Properties.Settings.Default.savePath = tSavePath.Text;
                Properties.Settings.Default.Save();
            }
            else
            {
                MessageBox.Show("Save path not valid.");
            }
        }
    }



    [Serializable]
    public class RaceCard
    {
        public string Date { get; set; }
        public string MeetingsNo { get; set; }

        [XmlArray("Meetings"), XmlArrayItem(typeof(Meeting), ElementName = "Meeting")]
        public List<Meeting> Meetings { get; set; }
    }

    [XmlRoot("Meeting")]
    [Serializable]
    public class Meeting
    {
        public string Name { get; set; }

        public string Date { get; set; }
        public string JumpTime { get; set; }


        [XmlArray("Races"), XmlArrayItem(typeof(Race), ElementName = "Race")]
        public List<Race> Races { get; set; }
    }

    [XmlRoot("Race")]
    [Serializable]
    public class Race
    {
        public string PostTime { get; set; }
        public string Class { get; set; }
        public string Distance { get; set; }

        [XmlArray("Runners"), XmlArrayItem(typeof(Runner), ElementName = "Runner")]
        public List<Runner> Runners { get; set; }
    }

    [XmlRoot("Runner")]
    [Serializable]
    public class Runner
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Sire { get; set; }
        public string Dam { get; set; }
        public string DOB { get; set; }
        public string Trainer { get; set; }


        [XmlArray("Last 5"), XmlArrayItem(typeof(Forma), ElementName = "Form")]
        public List<Forma> Forma { get; set; }


    }

    [XmlType(TypeName = "Forma")]
    [XmlRoot("Forma")]
    [Serializable]
    public class Forma
    {
        public string Date { get; set; }
        public string Distance { get; set; }
        public string RaceNumber { get; set; }
        public string Sectional { get; set; }
        public string Peer { get; set; }
        public string FP { get; set; }
        public string Comment { get; set; }
        public string WinnerTime { get; set; }
        public string TimeAdj { get; set; }
        public string Class { get; set; }
        public string RunnerTime { get; set; }
    }




}



