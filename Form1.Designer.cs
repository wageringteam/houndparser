﻿namespace HoundParser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtURL = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbLog = new System.Windows.Forms.TabPage();
            this.txtErrorLog = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.TabPage();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.tmrObrada = new System.Windows.Forms.Timer(this.components);
            this.btnParse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMeetingDelay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tSavePath = new System.Windows.Forms.TextBox();
            this.bSavePath = new System.Windows.Forms.Button();
            this.bGetAppPAth = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbLog.SuspendLayout();
            this.tbOutput.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtURL
            // 
            this.txtURL.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtURL.Location = new System.Drawing.Point(0, 0);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(800, 20);
            this.txtURL.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 321);
            this.panel1.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbLog);
            this.tabControl1.Controls.Add(this.tbOutput);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 321);
            this.tabControl1.TabIndex = 0;
            // 
            // tbLog
            // 
            this.tbLog.Controls.Add(this.txtErrorLog);
            this.tbLog.Location = new System.Drawing.Point(4, 22);
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(792, 295);
            this.tbLog.TabIndex = 1;
            this.tbLog.Text = "Log";
            this.tbLog.UseVisualStyleBackColor = true;
            // 
            // txtErrorLog
            // 
            this.txtErrorLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtErrorLog.Location = new System.Drawing.Point(0, 0);
            this.txtErrorLog.Multiline = true;
            this.txtErrorLog.Name = "txtErrorLog";
            this.txtErrorLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtErrorLog.Size = new System.Drawing.Size(792, 295);
            this.txtErrorLog.TabIndex = 7;
            // 
            // tbOutput
            // 
            this.tbOutput.Controls.Add(this.txtOutput);
            this.tbOutput.Location = new System.Drawing.Point(4, 22);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(792, 295);
            this.tbOutput.TabIndex = 0;
            this.tbOutput.Text = "Output";
            this.tbOutput.UseVisualStyleBackColor = true;
            // 
            // txtOutput
            // 
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(0, 0);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOutput.Size = new System.Drawing.Size(792, 295);
            this.txtOutput.TabIndex = 8;
            // 
            // btnParse
            // 
            this.btnParse.Location = new System.Drawing.Point(696, 344);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(100, 67);
            this.btnParse.TabIndex = 6;
            this.btnParse.Text = "Parse";
            this.btnParse.UseVisualStyleBackColor = true;
            this.btnParse.Click += new System.EventHandler(this.BtnParse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 351);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Preload Delay";
            // 
            // txtMeetingDelay
            // 
            this.txtMeetingDelay.Location = new System.Drawing.Point(86, 347);
            this.txtMeetingDelay.Name = "txtMeetingDelay";
            this.txtMeetingDelay.Size = new System.Drawing.Size(70, 20);
            this.txtMeetingDelay.TabIndex = 7;
            this.txtMeetingDelay.Text = "2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 394);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Save Path";
            // 
            // tSavePath
            // 
            this.tSavePath.Location = new System.Drawing.Point(84, 391);
            this.tSavePath.Name = "tSavePath";
            this.tSavePath.Size = new System.Drawing.Size(360, 20);
            this.tSavePath.TabIndex = 9;
            // 
            // bSavePath
            // 
            this.bSavePath.Location = new System.Drawing.Point(536, 391);
            this.bSavePath.Name = "bSavePath";
            this.bSavePath.Size = new System.Drawing.Size(66, 23);
            this.bSavePath.TabIndex = 11;
            this.bSavePath.Text = "Save";
            this.bSavePath.UseVisualStyleBackColor = true;
            this.bSavePath.Click += new System.EventHandler(this.bSavePath_Click);
            // 
            // bGetAppPAth
            // 
            this.bGetAppPAth.Location = new System.Drawing.Point(450, 391);
            this.bGetAppPAth.Name = "bGetAppPAth";
            this.bGetAppPAth.Size = new System.Drawing.Size(80, 23);
            this.bGetAppPAth.TabIndex = 12;
            this.bGetAppPAth.Text = "Get App Path";
            this.bGetAppPAth.UseVisualStyleBackColor = true;
            this.bGetAppPAth.Click += new System.EventHandler(this.BGetAppPAth_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 416);
            this.Controls.Add(this.bGetAppPAth);
            this.Controls.Add(this.bSavePath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tSavePath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMeetingDelay);
            this.Controls.Add(this.btnParse);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtURL);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Parser";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tbLog.ResumeLayout(false);
            this.tbLog.PerformLayout();
            this.tbOutput.ResumeLayout(false);
            this.tbOutput.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Timer tmrObrada;
        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.TextBox txtErrorLog;
        private System.Windows.Forms.TabPage tbOutput;
        private System.Windows.Forms.TabPage tbLog;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMeetingDelay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tSavePath;
        private System.Windows.Forms.Button bSavePath;
        private System.Windows.Forms.Button bGetAppPAth;
    }
}

